FROM ruby:2.3-slim
RUN apt-get update -qq && apt-get install -y build-essential
RUN gem install bundler
RUN mkdir -p /opt
WORKDIR /opt
COPY Gemfile Gemfile
RUN bundle install
COPY conf conf
COPY app.rb app.rb 
ENTRYPOINT ["bundle","exec","ruby","app.rb"]
